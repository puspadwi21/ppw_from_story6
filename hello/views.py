from django.shortcuts import render
from django.http import JsonResponse
from django.db import IntegrityError
from .models import Status
from .models import Registration
from .forms import StatusForm
from .forms import RegistForm
# from django.core.validators import validate_email

import urllib.request, json
import requests

# Create your views here.

def profile(request):
    return render(request, 'profile.html')

def listbooks(request):
	return render(request, 'books.html')

response={}
def index(request):
	response['setstatus'] = StatusForm()
	listStatus = Status.objects.all()
	response['StatusForm'] = listStatus

	form = StatusForm(request.POST or None)
	if(request.method == 'POST' and form.is_valid()):
		response['status'] = request.POST['status']
		listStatus = Status(status=response['status'])
		listStatus.save()
		return render(request, 'hello.html', response)
	else:
		return render(request, 'hello.html', response)

def databooks(request):
    data_json = requests.get('https://www.googleapis.com/books/v1/volumes?q=architecture').json()
    items = []
    for i in data_json['items']:
        info = {}
        info['title'] = i['volumeInfo']['title']
        info['img'] = i['volumeInfo']['imageLinks']['smallThumbnail'] if 'imageLinks' in i['volumeInfo'].keys() else 'Not Found'
        info['author'] = ", ".join(i['volumeInfo']['authors'])
        info['publishedDate'] = i['volumeInfo']['publishedDate'] if 'publishedDate' in i['volumeInfo'].keys() else 'Not Found'
        items.append(info)
    return JsonResponse({"data" :items})

def showregistration(request):
	form = RegistForm(request.POST)
	html = 'registration.html'
	return render(request,html,{'showform':form})

def saveregistration(request):
	response['showform'] = RegistForm()
	form = RegistForm(request.POST or None)
	if request.method == 'POST' and form.is_valid():
		response['email'] = request.POST['email']
		response['name'] = request.POST['name']
		response['password'] = request.POST['password']
		param = 'True'

		try:
			toStore = Registration(
				email=response['email'],
				name=response['name'],
				password=response['password']
			)
			toStore.save()
			return JsonResponse({'param':param})
		except IntegrityError as e:
			param = 'False'
			return JsonResponse({'param':param})
	elif request.method == 'GET' and form.is_valid():
		data_json = Registration.objects.all()
		items = []
		for i in data_json['items']:
			info={}
			info['email'] = i['email']
			info['name'] = i['name']
			items.append(info)
		return JsonResponse({"data" :items})
	param = 'False'
	return JsonResponse({'param':param})

def listsubscribers(request):
	response['showform'] = RegistForm()
	form = RegistForm(request.GET or None)
	if request.method == 'GET' and form.is_valid:
		data_json = Registration.objects.all()
		for i in data_json['items']:
			info={}
			info['email'] = i['email']
			info['name'] = i['name']
			items.append(info)
	return JsonResponse({"data" :items})

