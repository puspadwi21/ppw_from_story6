from django.db import models

# Create your models here.
class Status(models.Model):
	status = models.CharField(max_length=500)
	time = models.DateTimeField(auto_now_add=True)

class Registration(models.Model):
	email = models.EmailField(unique=True)
	name = models.CharField(max_length=100)
	password = models.CharField(max_length=100)