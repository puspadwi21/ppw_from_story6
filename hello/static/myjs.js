 $(document).ready(function(){ 
    myFunction();

    function myFunction() {
        myVar = setTimeout(showPage, 1000);
    }

    function showPage() {
      $("#loader").css("display", "none");
      $(".animate-bottom").css("display","block") = "none";
    }

    $.ajax({
        type : 'GET',
        url : 'listsubscribers',
        dataType : 'json',
        success : function(data) {
            var print ='<tr>';
                console.log(data);
                for(var i=1; i<=data.data.length; i++) {
                    print+= '<th scope ="row">' + i +'</th> <td>';
                    print+= data.data[i-1].name + '></td>';
                    print+= '<td>' + data.data[i-1].email + '<td>';
                    print+= '<button type="button" class="btn btn-danger" id="registunsubscribe">Unsubscribe</button> </td></tr>';

                }
                $('#bodysubscribers').append(print);
        }
    });

   $('.accordion').find('.accordion-header').click(function() {
    $(this).next().slideToggle();
    $('.panel').not($(this).next()).slideUp();
   });

   $("#changetheme").on({
        click: function(){
            $("body").css("background-image","url(https://images.wallpaperscraft.com/image/forest_hill_fog_111466_1280x720.jpg)");
            $("#profiletitle").css("color","#424E46");
            $("#profiletitle").css("text-decoration-color","#F0B851");
            $("#name").css("color","#F0B851");
            $("#birthday").css("color","#F0B851");
            $("#college").css("color","#F0B851");
            $(".accordion").css("background","#595858");
            $(".accordion-content").css("color","black");
            $(".accordion-header").css("color","white");
        },
        dblclick: function(){
            $("body").css("background-image","url(https://images.wallpaperscraft.com/image/forest_hill_fog_111466_1280x720.jpghttps://images.wallpaperscraft.com/image/northern_lights_aurora_winter_129726_3840x2160.jpg)");
            $("#profiletitle").css("color","white");
            $("#profiletitle").css("text-decoration-color","#FAFAD2");
            $("#name").css("color","white");
            $("#birthday").css("color","white");
            $("#college").css("color","white");
            $(".accordion").css("background","white");
            $(".accordion-content").css("color","black");
            $(".accordion-header").css("color","black");
        },

   });
   var counter=0;
   $.ajax({
        type : 'GET',
        url : 'api/books',
        dataType : 'json',
        success : function(data) {
            var print ='<tr>';
                for(var i=1; i<=data.data.length; i++) {
                    print+= '<th scope ="row">' + i +'</th> <td>'+data.data[i-1].title+'</td> <td>';
                    print+='<img src=' + data.data[i-1].img + '></td>';
                    print+= '<td>' + data.data[i-1].author;
                    print+= '</td> <td>' + data.data[i-1].publishedDate+'</td> <td>';
                    print+=' <button id="button" type="button" class="btn btn-outline-light"><i id="star"class ="fa fa-star"></i></button> </td></tr>';

                }
                $('#bodybooks').append(print);
        }
    });
   
    $(document).on('click', '#star', function() {
        if( $(this).hasClass('clicked') ) {
            counter -=1;
            $(this).removeClass('clicked');
        }
        else {
            $(this).addClass('clicked');
            counter = counter+ 1;


        }
        $('.counters').html(counter);

    });

    $('#registform').submit(function(e){
        e.preventDefault();
        $.ajax({
            method:'POST',
            url:'saveregistration',
            data:$('form').serialize(),
            success:function(status){
                if (status.param == 'True') {
                    $('#alert').html("<div class='alert alert-success' role='alert'>Anda terdaftar sebagai subscriber!</div>");
                }else{
                    $('#alert').html("<div class='alert alert-danger' role='alert'>Email sudah terdaftar sebelumnya, silahkan menggunakan email lain</div>");
                }
            }
    
        });
        return false;
    });

     $('#registsubmit').closest('form')
        .submit(function() {
            return checkForm.apply($(this).find(':input')[0]);
        })
        .find(inputSelector).keyup(checkForm).keyup();


});

const inputSelector = ':input[Required]:visible';

function checkForm() {
  var isValidForm = true;
  $(this.form).find(inputSelector).each(function() {
    if (!this.value.trim()) {
      isValidForm = false;
    }
  });
  $(this.form).find('#registsubmit').prop('disabled', !isValidForm);
  return isValidForm;
}

