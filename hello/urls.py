from django.urls import path
from .views import index
from .views import profile
from .views import listbooks
from .views import databooks
from .views import showregistration
from .views import saveregistration
from .views import listsubscribers
# from .views import validation

app_name ='hello'
urlpatterns = [
    path('', index, name='index'),
    path('profile', profile, name='profile'),
    path('listbooks', listbooks, name='listbooks'),
    path('api/books', databooks, name='books'),
    path('registration', showregistration, name='registration'),
    path('saveregistration', saveregistration, name='saveregistration'),
    path('listsubscribers', listsubscribers, name='listsubscribers'),
    # path(r'validation', validation, name='validation')
]

##
