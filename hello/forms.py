from django import forms

class StatusForm(forms.Form):
	attrs = {
		'type': 'text',
		'class': 'form-control'
	}
	status = forms.CharField(label="Status", max_length=300, required=True, widget=forms.TextInput(attrs=attrs))

class RegistForm(forms.Form):
	error_messages={
        'required':'Required Field'
    }
	email_attrs = {
		'type': 'email',
		'class': 'form-control',
		'placeholder' : 'Required'
	}
	name_attrs = {
		'type': 'text',
		'class': 'form-control',
		'placeholder' : 'Required'
	}
	password_attrs = {
		'type': 'password',
		'class': 'form-control',
		'placeholder' : 'Required'
	}
	email = forms.EmailField(label="Email", max_length=25, required=True, widget=forms.EmailInput(attrs=email_attrs))
	name = forms.CharField(label="Name", max_length=100, required=True, widget=forms.TextInput(attrs=name_attrs))
	password = forms.CharField(label="Password", max_length=20, required=True,widget=forms.PasswordInput(attrs=password_attrs))