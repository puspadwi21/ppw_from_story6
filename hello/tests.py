from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from django.db import IntegrityError
from django.http import JsonResponse

from .views import index
from .views import profile
from .views import listbooks
from .views import showregistration
from .views import saveregistration
from .models import Status
from .models import Registration
from .forms import StatusForm
from .forms import RegistForm

from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options

# Create your tests here.
class HelloUnitTest(TestCase):

	##LANDING PAGE
    def test_hello_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_hello_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_hello_contains_string(self):
    	request = HttpRequest()
    	response = index(request)
    	string = response.content.decode("utf8")
    	self.assertIn("Hello, Apa Kabar?", string)

    def test_hello_using_template(self):
    	response = Client().get('/')
    	self.assertTemplateUsed(response, 'hello.html')

    ##PROFILE PAGE
    def test_profile_url_is_exist(self):
    	response = Client().get('/profile')
    	self.assertEqual(response.status_code, 200)

    def test_profile_using_index_func(self):
    	found = resolve('/profile')
    	self.assertEqual(found.func, profile)

    ##BOOKS PAGE
    def test_books_url_is_exist(self):
        response = Client().get('/listbooks')
        self.assertEqual(response.status_code, 200)

    def test_hello_using_index_func(self):
        found = resolve('/listbooks')
        self.assertEqual(found.func, listbooks)


    def test_hello_using_template(self):
        response = Client().get('/listbooks')
        self.assertTemplateUsed(response, 'books.html')

    ##REGISTRATION PAGE
    def test_regist_url_is_exist(self):
        response = Client().get('/registration')
        self.assertEqual(response.status_code, 200)

    def test_regist_using_index_func(self):
        found = resolve('/registration')
        self.assertEqual(found.func, showregistration)

    def test_JSON_object_failed_created(self):
        create_model = Registration.objects.create(email='dwp@dwp.com',name='dwp',password='katasandiku')
        self.assertRaises(IntegrityError)

    def test_regist_submitting(self):
        name='dwp'
        email='dwp@dwp.com'
        password = 'katasandiku'
        response_post=Client().post('/saveregistration/',{'email':email,'name':name,'password':password})
        self.assertEqual(response_post.status_code,404)

    def test_regist_using_template(self):
        response = Client().get('/registration')
        self.assertTemplateUsed(response, 'registration.html')    

class HelloFunctionalTest(TestCase):
    def setUp(self):
        chrome_options=Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        service_log_path="./chromedriver.log"
        service_args=['--verbose']
        self.selenium=webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        self.selenium.implicitly_wait(25)
        super(HelloFunctionalTest,self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(HelloFunctionalTest,self).tearDown()

    def test_input_status_form(self):
        selenium=self.selenium

        #Opening the web url to be tested
        selenium.get('https://hello-dwp.herokuapp.com')

        #Finding the form
        status=selenium.find_element_by_class_name('form-control')
        submit=selenium.find_element_by_id('submit-button')

        #Filling form with data
        status.send_keys('Coba Coba')

        #Submitting form
        submit.send_keys(Keys.RETURN)
        self.assertIn('Coba Coba', selenium.page_source)

    def test_check_hello_title(self):
        selenium=self.selenium

        #Opening the web url to be tested
        selenium.get('https://hello-dwp.herokuapp.com')

        #Checking the page title
        self.assertIn('Hello!', selenium.title)

    def test_check_profile_id(self):
        selenium=self.selenium

        #Opening the web url to be tested
        selenium.get('https://hello-dwp.herokuapp.com/profile')

        #Checking the id(s)
        name=selenium.find_element_by_id('name')
        birthday=selenium.find_element_by_id('birthday')
        college=selenium.find_element_by_id('college')
        sasari=selenium.find_element_by_id('sasari')

        #Check All
        self.assertTrue(name, selenium.page_source)
        self.assertTrue(birthday, selenium.page_source)
        self.assertTrue(college, selenium.page_source)
        self.assertTrue(sasari, selenium.page_source)

    ##def test_check_background_color(self):
        ##selenium=self.selenium

        #Opening the web url to be tested
        ##selenium.get('https://hello-dwp.herokuapp.com')

        #Finding the background color which is put on the body element
        ##body=selenium.find_element_by_tag_name('body')
        ##background=body.value_of_css_property('background-color')

        #Check the background color
        #self.assertEqual('rgba(255, 218, 185, 1)', background)

    ##def test_check_button_color(self): 
        #selenium=self.selenium

        #Opening the web url to be tested
        #selenium.get('https://hello-dwp.herokuapp.com')

        #Finding the button
        #button=selenium.find_element_by_id('submit-button')
        #button_color=button.value_of_css_property('background-color')

        #Check the button color
        #self.assertEqual('rgba(92, 184, 92, 1)', button_color)
        
